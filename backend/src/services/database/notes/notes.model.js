module.exports = async function (app, tableName) {
    try {
        const db = app.get('knexClient');

        await db.schema.createTable(tableName, table => {

            table.increments('id');
            table.timestamp('created_at').defaultTo(db.fn.now());
            table.text('text').notNull();
        })

      console.log(`Created ${tableName} table`)

    }catch(err) {
      console.log(`Error creating ${tableName} table: ${err.toString()}`)

    }
}